#include <stdio.h>
#include <unistd.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

#ifndef max
    #define min(a,b) ((a) > (b) ? (b) : (a))
#endif

#define ES_MINPATTERN 3
#define ES_MAXKEYWORDCHAR 40
#define ES_MAXKEYWORDLINE 10

#define NUMFIRST_PATTERN 1
#define NUMLAST_PATTERN 2
#define DOT_PATTERN 3
#define PAGE_PATTERN 4

typedef struct sSummaryEntry
{
	char*	m_sKeywords;	// A relevant headline, typically at least first word, or the foirts line if less than 40 char (or 10 words)
	char*	m_sLine;		// Text that look like associated with a line
	 int 	m_nNumber;		// The Page number
}	sSummaryEntry;




char	*strSub(char *s, int start, int len)
{
	char	*tab;
	int		i;

	tab = (char *)malloc(sizeof(char) * (len + 1));
	if (!tab)
		return (NULL);
	i = 0;
	while (i < len)
	{
		tab[i] = s[start + i];
		i++;
	}
	tab[i] = '\0';
	return (tab);
}

char		*strCleanWhitespaces(char *s)
{
	int		len_start;
	int		len_end;
	int		i;
	int		len;
	char	*tab;

	len = strlen(s);
	i = 0;
	len_start = 0;
	while (isspace(s[i]))
		i++;
	len_start = i;
	i = len - 1;
	len_end = 0;
	while (isspace(s[i]) && i > len_start)
		i--;
	len_end = len - 1 - i;
	tab = (char *)malloc(sizeof(char) * (len - len_start - len_end + 1));
	if (!tab)
		return (NULL);
	i = 0;
	while (++i - 1 < len - len_start - len_end)
		tab[i - 1] = s[len_start + i - 1];
	tab[i] = '\0';
	i = 0;
	while (tab[i])
	{
		if (tab[i] == '\n')
			tab[i] = ' ';
		i++;
	}
	return (tab);
}

int		strEqual(char *dst, char *src, int len)
{
	int		i = 0;

	while (i < len)
	{
		if (dst[i] != src[i])
			return (0);
		i++;
	}
	return (1);
}

int		getLineLen(char *line, char c)
{
	int		len;

	len = 0;
	while (c != 'p' && c != '.')
	{
		if (isdigit(*line))
			break ;
		len++;
		line++;
	}

	while (c == '.')
	{
		if (*line == '.')
			break ;
		len++;
		line++;
	}

	while (c == 'p') //j'ai essayé d'utiliser strcmp strncmp strstr et strnstr sans succes
	{
		if (line[0] == 'p' && line[1] == 'a' && line[2] == 'g' && line[3] == 'e' && isspace(line[4]))
			break ;
		len++;
		line++;
	}

	return (len);
}

char	*goToNum(char *line)
{
	while (isdigit(*line))
		line++;
	while (!isdigit(*line) && *line)
		line++;
	if (*line == '\0')
		return (NULL);
	return (line);
}

int		getEntriesNum(const char *line, int maxPageNum)
{
	int		entries;

	entries = 0;
	while (*line)
	{
		if (isdigit(*line))
		{
			if (atoi(line) <= maxPageNum)
				entries++;
			while (isdigit(*line))
				line++;
		}
		line++;
	}
	return (entries);
}

char	*getLastString(char *line)
{
	int		i = 0;
	char	*str;

	str = (char *)malloc(sizeof(char) * (4 + 1));
	while (!isalpha(*line) && *line != '.')
		line--;
	while (i < 4)
	{
		str[i] = *(line - 3 + i);
		i++;
	}
	str[4] = '\0';
	return (str);
}

int		lenNum(int n)
{
	int		i = 0;

	while (n / 10 != 0)
	{
		n /= 10;
		i++;
	}
	return (i + 1);
}

int		getPattern(char *line, const int maxPageNum)
{
	char	*pNum;
	int		nNum;
	int		numFirstPattern = 0;
	int		numLastPattern = 0;
	int		dotPattern = 0;
	int		pagePattern = 0;
	int		flag;
	int		i;

	pNum = line;
	while ((pNum = goToNum(pNum)) != NULL)
	{
		nNum = atoi(pNum);
		while (isspace(*pNum))
			pNum++;
		pNum += lenNum(nNum);
		if (nNum > maxPageNum)   //si le nombre est a ignorer
			continue ;
		i = 0;
		flag = 0;
		while (pNum[i] != '\n' && pNum[i] != '\0')
		{
			if (isalpha(pNum[i]))
			{
				flag = 1;
				numFirstPattern++;
				break ;
			}
			i++;
		}
		if (strstr(getLastString(pNum), "page") && !flag)
			pagePattern++;
		else if (strstr(getLastString(pNum), "....") && !flag)
			dotPattern++;
		else if (!flag)
			numLastPattern++;
		if (numFirstPattern == ES_MINPATTERN || numLastPattern == ES_MINPATTERN || dotPattern == ES_MINPATTERN || pagePattern == ES_MINPATTERN)
			break ;
	}

	// printf("numFirstPattern %d  numLastPattern %d  dotPattern %d  pagePattern %d\n", numFirstPattern, numLastPattern, dotPattern, pagePattern);

	if (numFirstPattern == ES_MINPATTERN)
		return (NUMFIRST_PATTERN);
	else if (numLastPattern == ES_MINPATTERN)
		return (NUMLAST_PATTERN);
	else if (dotPattern == ES_MINPATTERN)
		return (DOT_PATTERN);
	else if (pagePattern == ES_MINPATTERN)
		return (PAGE_PATTERN);

	return (0);
}

void	extractNumFirstPattern(sSummaryEntry *pResult, char *pSummary, int nEntries, int maxPageNum)
{
	int		i = 0;
	int		textLen;

	while (i < nEntries - 1)
	{
		pSummary = goToNum(pSummary);
		while (atoi(pSummary) > maxPageNum)
			pSummary = goToNum(pSummary);
		pResult[i].m_nNumber = atoi(pSummary);
		while (isdigit(*pSummary))
			pSummary++;
		textLen = getLineLen(pSummary, 0);
		pResult[i].m_sKeywords = strCleanWhitespaces(strSub(pSummary, 0, textLen));
		i++;
	}
}

void	extractNumLastPattern(sSummaryEntry *pResult, char *pSummary, int nEntries, int maxPageNum)
{
	int		i = 0;
	int		textLen;

	while (i < nEntries - 1)
	{
		textLen = getLineLen(pSummary, 0);
		pResult[i].m_sKeywords = strCleanWhitespaces(strSub(pSummary, 0, textLen));
		pSummary = goToNum((char *)pSummary);
		while (atoi(pSummary) > maxPageNum)
			pSummary = goToNum(pSummary);
		pResult[i].m_nNumber = atoi(pSummary);
		while (isdigit(*pSummary))
			pSummary++;
		i++;
	}
}

void	extractDotPattern(sSummaryEntry *pResult, char *pSummary, int nEntries, int maxPageNum)
{
	int		i = 0;
	int		textLen;

	while (i < nEntries - 1)
	{
		textLen = getLineLen(pSummary, '.');
		pResult[i].m_sKeywords = strCleanWhitespaces(strSub(pSummary, 0, textLen));
		pSummary = goToNum((char *)pSummary);
		while (atoi(pSummary) > maxPageNum)
			pSummary = goToNum(pSummary);
		pResult[i].m_nNumber = atoi(pSummary);
		while (isdigit(*pSummary))
			pSummary++;
		i++;
	}
}

void	extractPagePattern(sSummaryEntry *pResult, char *pSummary, int nEntries, int maxPageNum)
{
	int		i = 0;
	int		textLen;
	char	*tmp;

	while (i < nEntries - 1)
	{
		tmp = pSummary;
		textLen = getLineLen(pSummary, 'p');
		pResult[i].m_sKeywords = strCleanWhitespaces(strSub(pSummary, 0, textLen));
		pSummary = goToNum((char *)pSummary);
		while (atoi(pSummary) > maxPageNum || (textLen > (int)(pSummary - tmp)))
			pSummary = goToNum(pSummary);
		pResult[i].m_nNumber = atoi(pSummary);
		while (isdigit(*pSummary))
			pSummary++;
		i++;
	}
}

// 		"nnn text \n text\n"		<- 3+ time this one
// 		"text \n text nnn\n"		<- 3 time this one
// 		"text " "..." " nnn\n"		<- "..." is the supect word => if pattern exist more than 3 time it is a summary model
// 		"text " "page nnn\n"		<- Page is keyword

//------------------------------- CPDF --------------------------------
//! 							
//!  \e Function :	ExtractSummary 
//!  \e Purpose  :	Analyse input string and if relevant extract a summary structure of it
//!
//!@param pSummary a lpsz string (ASCII so far) the scorched fruit
//!@param maxPageNum Number of Pages in teh document, a summary should not refere to number higher
//!@return The structure of the summary,, always return somtehing, last entry is m_sKeywords == NULL
//-----------------------------------------------------------------------

sSummaryEntry* ExtractSummary(const char* pSummary, const int maxPageNum)
{
	sSummaryEntry	*pResult;
	void			(*extract)(sSummaryEntry *, char *, int, const int);
	int				pattern;
	int				nEntries;

	nEntries = getEntriesNum(pSummary, maxPageNum);
	pResult = (sSummaryEntry *)malloc(sizeof(sSummaryEntry) * (nEntries + 1));

	pResult[nEntries].m_sKeywords = NULL;
	pResult[nEntries].m_sLine = NULL;
	pResult[nEntries].m_nNumber = 0;

	pattern = getPattern((char *)pSummary, maxPageNum);
	if (pattern == NUMFIRST_PATTERN)
		extract = extractNumFirstPattern;
	else if (pattern == NUMLAST_PATTERN)
		extract = extractNumLastPattern;
	else if (pattern == DOT_PATTERN)
		extract = extractDotPattern;
	else if (pattern == PAGE_PATTERN)
		extract = extractPagePattern;
	else
		return(pResult);

	extract(pResult, (char *)pSummary, nEntries, maxPageNum);
	return(pResult);
}
	/*
		INPUT
		Read the input string and search for a pattern that help extracting a summary info.
		Patterns that are candidate are;
		"nnn text \n text\n"		<- 3+ time this one
		"text \n text nnn\n"		<- 3 time this one
		"text " "..." " nnn\n"		<- "..." is the supect word => if pattern exist more than 3 time it is a summary model
		"text " "page nnn\n"		<- Page is keyword
		
		Note:
		Ruling can be filtered with relancy concepts
		- It is not expected to see a mix of patterns, so that once one is found it can generically be used
		- Number should be < 3000 (in fact < maxPageNum)
		
		OUTPUT
		A Table of sSummaryEntry
		Zero Terminated. otherwise said, last entry have m_sKeywords==NULL m_sKeywords==NULL m_nNumber == 0
		which will be used to enumerate the number of entries
		
		Exemple
		
>		l’essentiel de la franchise 
08 La franchise
 Forme moderne du commerce organisé
 10 “Il faudra adhérer à des règles
 et contribuer à une vie de réseau”
 12 Êtes-vous fait pour la franchise ?
 14 Comment lire un contrat
 de franchise ?
 18 Le franchisé face au secret
 du savoir-faire
 22 Le DIP : un élément essentiel
 à ne pas négliger
 24 La franchise oui…
 mais quel secteur ?
 26 Contrats
 Comment s’y retrouver ?
 28 Le franchiseur a-t-il les reins solides ? 
30 Trombinoscope
 à l’usage d’un futur franchisé
 34 La FFF, porte-parole
 des entrepreneurs en franchise
 36 La loi Doubin
 40 Que vaut le savoir-faire
 du franchiseur
 42 Signé, c’est signé !<	
	*/


void QUALIFY_ExtractSummary(void)
{

	char	*firstPattern = "01 l’essentiel de la franchise \n 08 La franchise\n Forme moderne du commerce organisé\n\
 10 “Il faudra adhérer à des règles\n et contribuer à une vie de réseau”\n 12 Êtes-vous fait pour la franchise ?\n\
 14 Comment lire un contrat\n de franchise ?\n 18 Le franchisé face au secret\n du savoir-faire\n\
 22 Le DIP : un élément essentiel\n à ne pas négliger\n 24 La franchise oui…\n mais quel secteur ?\n 26 Contrats\n\
 Comment s’y retrouver ?\n 28 Le franchiseur a-t-il les reins solides ? \n 30 Trombinoscope\n à l’usage d’un futur franchisé\n\
 34 La FFF, porte-parole\n des entrepreneurs en franchise\n 36 La loi Doubin\n 40 Que vaut le savoir-faire\n du franchiseur\n\
 42 Signé, c’est signé !";



 // 	char	*pagePattern = "l’essentiel de la franchise    page 01\n\
 // La franchise\n\
 // Forme moderne du commerce organisé    page 08\n\
 // “Il faudra adherer à des regles\n\
 // et contribuer à une vie de reseau” 	page 10\n\
 // Etes-vous fait pour la franchise ?		page 12\n\
 // Comment lire un contrat\n\
 // de franchise ?		page 14\n\
 // Le franchisé face au secret\n\
 // du savoir-faire	page 18\n\
 // Le DIP : un élément essentiel\n\
 // à ne pas négliger		page 22\n\
 // La franchise oui…\n\
 // mais quel secteur ?		page 24\n\
 // Contrats\n\
 // Comment s’y retrouver ?	page 26\n\
 // Le franchiseur a-t-il les reins solides ?  		page 28\n\
 // Trombinoscope\n\
 // à l’usage d’un futur franchisé		page 30\n\
 // La FFF, porte-parole\n\
 // des entrepreneurs en franchise		page 34\n\
 // La loi Doubin		page 36\n\
 // Que vaut le savoir-faire\n\
 // du franchiseur		page 40\n\
 // Signé, c’est signé !	page 42";


 
 // char	*lastPattern = "l’essentiel de la franchise 	01\n\
 // La franchise\n\
 // Forme moderne du commerce organisé 	08\n\
 // “Il faudra adhérer à des règles\n\
 // et contribuer à une vie de réseau” 	10\n\
 // Êtes-vous fait pour la franchise ?		12\n\
 // Comment lire un contrat\n\
 // de franchise ?		14\n\
 // Le franchisé face au secret\n\
 // du savoir-faire	18\n\
 // Le DIP : un élément essentiel\n\
 // à ne pas négliger		22\n\
 // La franchise oui…\n\
 // mais quel secteur ?	 24\n\
 // Contrats\n\
 // Comment s’y retrouver ?	26\n\
 // Le franchiseur a-t-il les reins solides ?  	28\n\
 // Trombinoscope\n\
 // à l’usage d’un futur franchisé	30\n\
 // La FFF, porte-parole\n\
 // des entrepreneurs en franchise	34\n\
 // La loi Doubin	36\n\
 // Que vaut le savoir-faire\n\
 // du franchiseur		40\n\
 // Signé, c’est signé !	42";

 
	// char	*dotPattern = "l’essentiel de la franchise  .............  01\n\
 // La franchise\n\
 // Forme moderne du commerce organisé  .............  08\n\
 // “Il faudra adhérer à des règles\n\
 // et contribuer à une vie de réseau”  .............  10\n\
 // Êtes-vous fait pour la franchise ?  .............  12\n\
 // Comment lire un contrat\n\
 // de franchise ?  .............  14\n\
 // Le franchisé face au secret\n\
 // du savoir-faire.............  18\n\
 // Le DIP : un élément essentiel\n\
 // à ne pas négliger.............22\n\
 // La franchise oui...\n\
 // mais quel secteur ?  .............  24\n\
 // Contrats\n\
 // Comment s’y retrouver ?  .............  26\n\
 // Le franchiseur a-t-il les reins solides ?  .............  28\n\
 // Trombinoscope\n\
 // à l’usage d’un futur franchisé.............  30\n\
 // La FFF, porte-parole\n\
 // des entrepreneurs en franchise.............  34\n\
 // La loi Doubin.............  36\n\
 // Que vaut le savoir-faire\n\
 // du franchiseur.............  40\n\
 // Signé, c’est signé !.............  42";


	sSummaryEntry*	pSummary = ExtractSummary(firstPattern, 120);
	sSummaryEntry*  pSum = pSummary;		// always return at least one

	int nCnt = 0;
	int nPage = 0;
	while (pSum->m_sKeywords)
	{
		assert(pSum->m_sKeywords && strlen(pSum->m_sKeywords) > 3);
		assert(pSum->m_nNumber > nPage);
		nPage = pSum->m_nNumber;

		printf("m_sKeywords |%s|   m_nNumber %d\n", pSum->m_sKeywords, pSum->m_nNumber);
		nCnt++;
		pSum++;
	}
	assert(nCnt == 14);
	printf("passed the test\n");
	// free(pSum);
	//TODO do rest of qualifier Analyse at least page numbering sequence (growing) and a keyword
}

int		main(void)
{
	QUALIFY_ExtractSummary();
	return (0);
}
